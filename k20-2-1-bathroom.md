# Bathroom (or K20-2-1)

This room acts as a bathroom which should mainly be used for showering.

### Facilities
* Bathtub with warm shower
* In-use towel drying station
* Toilet (tap-water driven) with bad flushing behavior
* Sleeping bag and blanket storage
* Handwashing basin (cold) with mirror
* Towel storage (in the small adjacent room)
* Dirty laundry basket (in the small adjacent room)
* laundry racks for when it is raining outside (in the small adjacent room)

### Door
Using the door of this room is quite loud, which is especially disturbing when people are already sleeping in the rooms around. A pragmatic solution is to always keep it open! :)  
If you want to close it for having privacy in the room, please try to do so silently (e.g. slow or by pushing the door slightly orthogonally to the moving direction).

### Showering
The shower uses the solar energy generated on the roof of the shed you can clearly see when looking outside. If it's nice and sunny you'll probably have the heat of the water for free! :) If solar power is not enough to generate the necerssary heat, a [Clage MCX 7 6.5 kW electrical passthrough heater](https://www.clage.de/de/Produkte/E-Kleindurchlauferhitzer/MCX) is in effect. You operate it normally by just turning on the hot water tap in the shower. The passthrough heater is temperature regulated. When it is turned off, you can use the push button to change the temperature:

* ECO (35 degrees)
* COMFORT (38 degrees)
* MAX (45 degrees)

Because of the low power of the passthrough heater, when you use MAX, you cannot turn on the tap fully.
The device will show that it does not reach the specified temperature when it starts blinking while the water is running.
It is advised to keep it at **COMFORT** and also put it back to that after you used it.
That is a comfortable shower temperature with high comfort as the temperature stays quite constant.

When you shower, make sure that the shower curtain is hanging inside the shower, so the water does not run on the floor.

Also, please either dry yourself inside the shower or use the towel laying on the bathtub (or a new one if there is none) to put on the floor in front of the shower.

![Don't: Wet floor because somebody did not dry inside the shower AND did not use a foot towel](images/bathroom-wet-floor.jpg)<br />
_Example of a wet floor. Walking on this with slightly dirty shoes/feet/socks will spread a lot of dirt everywhere!_

After you showered, the room needs to be ventilated to bring the moisture down.

### Heating
This room is only ever used shortly, so the heat is mostly lost.
Please do not heat this room!

An exception is in the cold winter: If it is below zero for prolongued time periods, keep the radiator set to "\*" so the mould stays away.
