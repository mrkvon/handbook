# Calendar

The digital calendar for Kanthaus.

### What is it for?
It's for recording events interesting to all Kanthausers. There are multiple sub-calendars, which respectively record people coming and going (people), where [our van](keinmuellwagen.md) is at (KMW), the schedules of various events in the city (wurzen), our recurring internal events (regular), the waste collections (müll) and one-time events we host or participate in (main).

### Who has access?
It's internal and not publicly visible. So far just a pretty random handful of people has access. In the future it should be linked to status in the house, meaning that you get an account when becoming Volunteer.
