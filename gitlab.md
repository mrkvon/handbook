# GitLab

We use GitLab to store mutable data. For immutable files we have a [Nextcloud](nextcloud.md)

### What is GitLab?
GitLab is basically a social network built on top of the [git technology](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics) for version control. It is normally used for code, because it makes it easy to collaborate on common projects without breaking anything.

### Why did you decide to use this?
- We want to help people learn things and appreciate technology!
- Data that was put in git is extremely secure, because it's stored on multiple people's computers.
- If multiple people work on the same file it's possible to tell who changed which exact character.
- It's easy to put content from GitLab onto our website.
- Everybody becomes a webmaster. :)
- The people who decided on this were already used to it... ;)

### What is stored where?
So far we have four repositories:
- [kanthaus-private](https://gitlab.com/kanthaus/kanthaus-private)
- [kanthaus-public](https://gitlab.com/kanthaus/kanthaus-public)
- [kanthaus-ansible](https://gitlab.com/kanthaus/kanthaus-ansible)
- [handbook](https://gitlab.com/kanthaus/handbook)

The first one contains the interesting data. This is where we store our [residence record](residencerecord.md), internal meeting minutes, financial plans and other stuff that contains personal information. Everything of relevance that consists of text should be put here.

As the name suggests, the second one is publicly accessible. So go ahead and click around to see what we have in there! The plan is to get rid of the public repo altogether and put the whole content directly on the website. We just didn't get around to do it yet...

Kanthaus-ansible is for keeping our server tidy. Nothing you need to worry about, except you're actually interested. In that case feel free to ask Matthias about it!

'Handbook' is where this handbook is stored.

### Anything else?
We also use GitLab issues for task tracking. Not for every tiny everyday task of course, but for the bigger [roadmap plannings](roadmap.md).
