# Dining Room (or K20-1-3)

The dining room (formerly called living room) is a room with a lot of table and sitting space, to accommodate many people eating.

### Facilities
- Two extensible tables with either 4-6 or 8-10 places.
- A large but dynamically changing number of chairs.
- An extensible sofa.
- A cupboard that holds games, books, notions, tobaccos and much more...

### Room availability
This is a public room, so it is always accessible to people. If someone decides to sleep in here, they shouldn't be surprised to be woken up by people coming in in the morning and having breakfast.

### Specials
Since this is one of the most public rooms, please make sure to keep the tables free of personal belongings if they are not in immediate use!
