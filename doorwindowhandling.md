# Door and window handling

This page describes doors and windows in the house with regards to

* why/when to open/close them
* physical handling (ToDo)


### Why and when to open and/or close doors?

Opening and closing doors permits/restricts **Air flow** as well as **noise**.

You want to have the most air flow when [airing](airing.md), so open the doors for that.
You don't want to have air flow when heating, so keep doors to heated areas closed:

* Heat leaves the heated room -> it would get colder
* With the warm air, humidity leaves into a cold room where it is easy to start condensation/mold groth ([See airing](airing.md))

You may want to close doors when you plan to be noisy in a room:
* Active conversation/discussion
* Working with tools that make noise
* Listening to music not knowing if people in neighboor rooms like that

### What does a closed door not mean?

The philosophy of Kanthaus focuses on openness, not isolation.
When a door is closed, it is mostly because of the reasons listed above.

**A closed door should always be seen as an invitation to come in and close it behind yourself again**

... But there is an exception: Kanthaus should provide for individual needs of silence, retreat and personal time.
The K20-2-5 private sleeping room has a sign to mark it as "please do not come in".
If you have this need for another room as well, speak to the people around you.
Feel always free to communicate your need for privateness, try to respect other peoples' need of privateness!

### Why and when to open/close a window?

When it is colder inside than outside, we like to keep windows closed, especially when the heating system is running.
Still, [airing](airing.md) multiple times a day to keep humidity/condensation low is necessary.

Please read in the appropriate page for details.

When it is warm outside, windows might be kept open when

* it is ensured that windows don't flap because of wind (one window already broke because of that)
* people inside don't mind increased noise
* our music/noise/being loud does not disturb neighbors outside
