# Summary

* [Introduction](README.md)
* [Handbook](handbook.md)

### Rooms
* [Room numbering](roomnumbering.md)
* [K20-0-2: Silent Office](silentoffice.md)
* [K20-2-1: Bathroom](k20-2-1-bathroom.md)
* [K20-1-2: Main Kitchen](kitchen.md)
* [K20-1-3: Dining Room](diningroom.md)
* [K20-2-3: Communal sleeping room](communalsleeping.md)
* [K20-2-4: Communal closet](communalcloset.md)

### Practicalities
* [Airing](airing.md)
* [Dishwashing](dishwashing.md)
* [Door and window handling](doorwindowhandling.md)
* [Hot drinks](hotdrinks.md)
* [Laundry](laundry.md)
* [Showering](k20-2-1-bathroom.md)

### Technical Infrastructure
* [Grey water system](greywatersystem.md)
* [House Bus](housebus.md)
* [KeinMüllWagen](keinmuellwagen.md)
* [Printing](printing.md)

### Digital Infrastructure
* [GitLab](gitlab.md)
  * [Residence record](residencerecord.md)
* [Nextcloud](nextcloud.md)
  * [Calendar](calendar.md)

### Social Infrastructure
* [Core documents](documents.md)
* [Positions and evaluations](positionsandevaluations.md)
