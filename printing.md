# Printing

There are multiple printers in the house that are available to be used.

* A Brother HL2140 black/white Laser printer
* A EPSON WF-3620 color inkjet printer (With scan, fax and copy capability)

## Installing the Brother HL2140

This printer is available on the network via *Raw printing*, *JetDirect*, *AppSocket* or *PDL Datastream* (which all refer to the same protocol, depending on whom you ask).
It is connected to the fritzbox via USB.
The printer does not have to be turned on to install it.
It will not be auto-detected by your system.

### Linux

* Install the appropriate driver
  * Arch/Manjaro: `brother-hl2140` from AUR or a collection of `foomatic-db`, `foomatic-db-engine` and `foomatic-db-ppds` (read in the [Wiki](https://wiki.archlinux.org/index.php/CUPS/Printer-specific_problems#Brother) for more details)
  * Ubuntu/Debian: Install `foomatic`
* Add the printer via your system utilities or via the generic cups web backend ([http://localhost:631]).
  * Chose `Add Printer`, `Other Networkprinter` and then `IPP` or `AppSocket` or anything else
  * Use `socket://fritz.box:9100` as the path of the printer
  * Give it a handy name. That is just for you to recognize it
  * Do not share it in the Network
  * Select `Brother` as the printer manufacturer and any of the following list as the model:
    * `HL-2140 Foomatic/hpijs-pcl5e`
    * `HL-2140 Foomatic/hl1250`
    * `Brother HL2140 for CUPS`

### Windows

* Download the appropriate driver from [Brother](http://support.brother.com/g/b/downloadtop.aspx?c=de&lang=de&prod=hl2140_all)
* Unpack or Install it, depending on what you downloaded
* Follow any Online tutorial for you windows (Hint: 7-10 are quite similar in the menu), e.g. [YouTube](https://www.youtube.com/watch?v=lcQbZeRn2k8)
  * Add a new Network Printer at `fritz.box` or `192.168.178.1`
  * Use "Custom" where it offers you "Generic Network Card". Make sure, the port is set to 9100
  * Select the Brother HL2140 when it asks you. If it is not in the list, point it to the downloaded & extracted driver above

## Installing the Epson WF-3620

This printer is available on the network but should also be auto detected.
Turn it on and use the printer administration tool of your operating system to use it.

### Linux

* You might need to download a driver
  * Arch/Manjaro: `epson-inkjet-printer-escpr` from AUR
  * Debian/Ubuntu: `epson-inkjet-printer-escpr`

### Windows

* Download the appropriate driver from [Epson](https://epson.com/Support/Printers/All-In-Ones/WorkForce-Series/Epson-WorkForce-WF-3620/s/SPT_C11CD19201) if it is not available already on your system.

## Using Brother HL2140

This printer prints single sided A4 paper with black/white in quite okay quality.
If you want to print double sided (duplex),
* print all even pages first
* take out the paper and put it into the paper load again, text facing up and readable (folding on the left)
* print out all odd pages

## Using Epson WF-3620

This printer is able to print with duplex by itself.
The quality is quite bad on the default settings.
To print with high quality (might take very long!), select "Epson UltraGlossy" as paper and a higher print quality.
Unfortunately, the yellow color seems to be broken (ink head stuck? We don't know. Maybe you want to find out?)

## Scanning with Epson WF-3620

There is a USB stick connected to this printer, it is most convenient to scan onto it.
Scanning can work via document feed on top of the printer (only for nicely looking A4 paper) or via manually scanning one by one in the scan top.

To scan,
* Insert your paper.
* Use EITHER
  * The Preset button to select a 200dpi singlesided/double sided preset scan options (good for documents).
  * The Scan button (touch display) and set everything yourself.
* Press the BW button to start the scan using grayscale.
* OR Press the Color button to start the scan using color.

The USB stick is available as a network share.
Use the file explorer of your choise to find the device called "Epson" something in the network (Samba file share).
To connect to it, use the account `guest` with any non-empty password.
The files are in the `EPSCAN` folder.
Unfortunately, you cannot remove them.
If you want to remove them, plug the USB stick in your local computer, but **please put it back to the printer afterwards**
