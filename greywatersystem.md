# Grey Water System

We collect grey water (e.g. from washing machine) to use it to flush the toilets, as it is still a valuable resource and helps reducing the amount of used water by ~30 percent.

### Overview

* 750 liters tank in K20-B-1 (the yellow one in the very small room) gets filled from the washing machine in K20-0-1
* A pump connects to a 220 liters buffer tank in K20-2#
* The toilets K20-0# and K20-1# are connected to the same line; the buffer uses gravity to refill the toilets

### Operating it

Currently, our grey water system needs **daily manual operation** to keep it running.
Don't worry: If you don't act on it, tab water is used instead, only **the washing machine must not be operated because tanks will overflow**

There is just three tasks:

* Keep storage tank from overfilling
* Keep buffer tank filled
* Regularly clean inlet filter next to the washing machine

#### Keep storage tank from overfilling

The washing machine must only be operated, when there is approximately 25cm of space in the storage tank.
There is no automatic protection against overfilling, the basement will just be flooded with the rest.

#### Keep buffer tank filled

The buffer tank needs to be manually filled by operating the pump and the valve next to the pump (both on the storage tank in K20-B-1).

1. Check how much space there is in the buffer tank, you find it in K20-2# after you pass by the dust barrier
2. Open the green valve next to the pump fully
3. Turn on the pump by switching the red switch to on
4. Turn off the pump before the buffer tank overfills, you can detect this in [Grafana (Greywater tank top line going up)](https://grafana.yunity.org)  or approximate it by timing: Filling the buffer completely takes approximately 4 minutes 30 seconds.
5. Close the valve, as otherwise the buffer is running empty again (ToDo: Put a backflow protection valve underneath the pump to avoid steps 2/5)

When the tank top fill level is sensed, there is still a bit of space, you have ~30 seconds to turn off the pump.

If the buffer tank overfills, there will be a lot of water running into the flooring and walls of K20-2#, that should be avoided!

#### Regularly clean inlet filter next to washing machine

To save toilets and pump from fabric leftovers, there is a filter next to the washing machine.
After each machine, it needs to be unscrewed and the fabric leftovers can be cleaned with the brush on the bathtub.
When screwing it back in, make sure that the filter inlet stays in the middle so it properly closes.

Sometimes, it might happen that the filter clogs even when it was cleaned, you can notice this by

* the washing machine giving an error
* the laundry feeling quite wet although the washing machine is done

In that case, clean the filter and depending on how clean the water in there looked either start another rinse (Spülen) or just spin-drying (Schleudern)
