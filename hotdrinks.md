# Hot Drinks

There are multiple ways to prepare hot drinks at Kanthaus. Here is how to go about them:

### Tea and coffee
- The tea and coffee corner is in the middle of the used dishes shelf. That's where you normally find the teapots. If we have already used tea that can be recycled it will also be stored here.
- Tea leaves, tea bags, herbs and coffee can be found underneath and above the microwave.

### Boiling water
- It's highly appreciated to make tea for many people! Especially in the morning it makes sense to boil bigger amounts of water. It's the privilege of the person preparing the hot drinks to choose which kind will be made.
- Use the huge green or blue pots for bigger amounts of water and make sure to put the lid on. For smaller amounts the electric kettle is fine.

### Brewing tea
- Please economize tea!
  - The used tea in the used dishes shelf can be used again.
  - One fresh teabag is more than enough for one liter of water, especially if you let it sit for a while.
- Please don't put loose tea directly into the teapots!
  - We have various tea sieves that can be put into the pot you boiled the water in. Just let the tea sit there and refill it afterwards.
  - If you make tea from ginger or lemon grass you should strain the tea through a small sieve while refilling it into the teapots.

### Brewing coffee
- I, Janina, never drink coffee, so someone else should write this section... Maybe Nick or Matthias? :)
