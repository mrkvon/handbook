# Nextcloud

We have our own instance of Nextcloud hosted on our server to store immutable data and our calendar.

### Who can reach it where?
The address is: https://cloud.kanthaus.online

You need to have an account to access the Nextcloud. So far only ten people have one, the plan is to link access to the position, so that every member and volunteer gets an account.
