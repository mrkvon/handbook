# Communal Closet (or K20-2-4)

The communal closet is a room for shared and personal clothes and belongings.

### Facilities
- The **main shared wardrobe** on the left contains most clothes.
  - The main shared wardrobe consist of three individual wardrobes, which contain from left to right–
    - T-shirts, tops, long sleeves
    - Thin and special jumpers, zipper jackets, skirts
    - All kinds of pants, dresses
  - The clothes in each wardrobe are sorted from top to bottom and from left to right following a 'bigger to smaller' pattern.
- On top of the main shared wardrobe there are boxes for **long-term storage of personal belongings**.
  - Items stored here won't be touched without permission and/or a very good reason.
- The **secondary shared wardrobe** on the right contains more clothes and bedclothes.
  - The open part contains from right to left–
    - Panties in the bag nailed to the side
    - Bras in the bag behind the panty bag
    - Smaller socks in the crate on the bottom
    - Boxers in the crate on the bottom
    - Bigger socks in the crate on the bottom
    - Jumpers on the hangers
    - Scarves left of the jumpers
    - Hankies, gloves, legwarmers and hats in the top department
  - The closed part next to the window contains from top to bottom–
    - Pillow cases (80x80cm on the left and other sizes on the right)
    - Sheets (small sheets on the left, big sheets on the right)
    - Blanket covers (smaller ones on the left, bigger ones on the right)
    - More blanket covers (unsorted, probably small)
    - Various textiles (tablecloths, decorative cloths, sheets without rubber bands)
- On top of the secondary shared wardrobe there is the **long-term backpack storage** area.
  - Empty and/or unused backpacks can be put here.
- In the middle of the room there are **mid-term individual storage facilities**.
  - For personalized items of frequent use.
  - Compartments can be claimed by putting a named label on them.
  - Preferably not more than 1-2 compartments should be claimed by one person.
  - When leaving for longer than a few days these storage facilities should be emptied, so that they can be used by other people. Personalized stuff can be transferred to a box and put on top of the main shared wardrobe.
- Left and right of the door to the hallway there are the **night storage shelves**.
  - Each shelf has four compartments with two hooks.
  - The compartments here are personalized if a name tag is put onto them.
  - They are meant for people to put their day-clothes before going to bed in the [communal sleeping room](communalsleeping.md).

### Room availability

This room should always be available.

### Specials

This is the room to put clean laundry. If you don't want to sort it in, just put the box in front of the individual storage facilities.  
This is also a room where people change clothes and may run around (almost) naked. No need to freak out about this... ;)
