# Laundry

Everything concerning laundry - be it dirty or clean.

### Dirty laundry sorting

Dirty laundry can be put into the laundry containers on each floor (K20-X-1b) or directly sorted into the fitting sections in K20-0-1b. Matthias takes laundry very seriously, so be sure to sort it right or just put it in an unsorted container.

Operating the washing machine touches the status of the [grey water system](greywatersystem.md) and the weather. If the weather forecast foresees rain, it is better not to do laundry.

### Wet laundry

Wet laundry is to be put up to dry outside. There are clothes pegs directly on the washing line. Make sure to adequately fix the laundry to the washing line, so that even stronger wind will not make it fall down and get dirty again.

### Dry laundry

Take a clean green box from the washing room and take down dry laundry. Make sure to leave the clothes pegs on the washing line. If you don't want to sort it in the [communal closet](communalcloset.md), just put the whole box there.
