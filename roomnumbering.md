# Roomnumbering

As the Kanthaus consist of two houses with multiple levels per house and multiple rooms per level, there is the need for a systemic numbering approach.
The numbering is split up into different sections for each part:

* House
* Level
* Room

This makes up for a reference like `K20-0-2` for the silent office or `K22-1-1` for the food storage room.

### Houses

The houses are identified as:

* K20
* K22

### Floors

The floor you enter a building on from the street or courtyard is called **ground floor** or numbered 0.

* B: Basement
* 0: Ground floor
* 1: Floor directly above ground floor
* 2: Top usable floor
* 3: Attic (not usable!)

### Rooms

Individual rooms in the specified section are identified by numbering them **counter clock wise** starting from the point where you enter the floor's hallway to the right with 1.

The room numbering is individual for most floors but generally is in the range from 1 to 5.

### Exceptions

* Hallways: Don't have numbers. Referenced as (e.g.) `K20-1 hallway`
* Staircase: Don't have numbers. Reference as (e.g.) `K20 staircase`
* Half-Floor rooms in staircases: `K20-0#` is the room half way between `K20-0` and `K20-1`
* The small "rooms" behind the bathrooms are not numbered