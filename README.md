# :wave: Introduction

Welcome to the [kanthaus](https://kanthaus.online) handbook :smile:

It is the user guide for people in the house who want to know how things work around here.

It doesn't have much content yet, but hopefully will contain things like:
* how food preparation works
* how to use the communal clothing
* how you can best contribute whilst you are here
* how to understand the role system (Visitor, Volunteer, Member)
* much more :)

Currently the website has some content that relates to these topics too, see:
* https://kanthaus.online/en/governance/governancefaq
* https://kanthaus.online/en/governance/collectiveagreements
* https://kanthaus.online/en/governance/positionsandevaluations

If you want to contribute, head over to the [handbook](handbook.md) section :smile: