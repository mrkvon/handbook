# Silent Office (or K20-0-2)

The silent office is a space for silent office work on your own projects.
It provides desk space for 7 people, including a bunch of monitors, keyboards and mice.

### Facilities

* Desks (three double desks, one single desk)
* Monitors (1x 27" mostly used by Nick, 2x 24" mostly used by Tilmann/Matthias, some 17-19" ones available)
* One US Keyboard (mostly used by Matthias)
* 2-3 Mice (mostly in use)
* Water tab for filling cups
* small storage space underneath some desks

### Room availability

The silent office should be available to people wanting to not be disturbed when working.
The room can be booked for certain events (e.g. Hackathons) that might relieve the non-disturbance rule to their own needs.
Otherwise, it should be available for anybody to work in here.


### How to handle private things/desk clutter

If you reside in Kanthaus mainly for working on a specific thing, you might want some working material always available.
It might come convenient to leave your paperwork/usb stick/cup on the desk while you go for food, for a walk or for sleep.
This is okay, as long as you plan on using the same desk soon again and use it continuously for multiple hours in a block.

If you are only spontaneously looking for a space here, please take away all the items you brought with you when leaving the room.

There is limited storage space available underneath some desks; this should be available to the people who work in this room regularly and might need access to those.

### Specials

* The radiator in this room can be kept at 3-5 when warmth is wanted. The radiator in this room is relatively small so this room will unlikely get too hot as the heating system is limited/controlled globally. Turn it to 0 when the room will be empty for some hours.